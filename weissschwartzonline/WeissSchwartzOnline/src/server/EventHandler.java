package server;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.Vector;

import exceptions.UnableToAtack;
import items.cards.Card;
import items.cards.Character;
import items.cards.Climax;
import items.cards.effectInterface.ActAbilityActivated;
import items.cards.effectInterface.AutoAbilityActivated;
import items.cards.effectInterface.Bond;
import items.cards.effectInterface.CardDrawn;
import items.cards.effectInterface.CharacterRested;
import items.cards.effectInterface.ClockToWaitingRoom;
import items.cards.effectInterface.Counter;
import items.cards.effectInterface.Effect;
import items.cards.effectInterface.Encore;
import items.cards.effectInterface.FieldToHand;
import items.cards.effectInterface.FieldToWaitingRoom;
import items.cards.effectInterface.HandToStage;
import items.cards.effectInterface.HandToWaitingRoom;
import items.cards.effectInterface.LevelUp;
import items.cards.effectInterface.WaitingRoomToField;
import items.cards.effectInterface.WhenAttack;
import items.cards.effectInterface.WhenReversed;
import items.field.Player;

public class EventHandler {
	public final static int FRONT = 0;
	public final static int SIDE = 1;
	public final static int DIRECT = -1;
	public final static int NONE = 2;
	public final static int FREE = 3;
	private static Map<String, Vector<Effect>> effects = new HashMap<String, Vector<Effect>>();
	private static Vector<Effect> stack = new Vector<Effect>();

	// Environmental
	public static boolean encoreAble() {
		// TODO Auto-generated method stub
		return false;
	}

	// PURE HANDLERS
	public static void bond(Card c) {
		for (Effect e : effects.get("Bond")) {
			Bond k = (Bond) e;
			if (k.check(c)) {
				k.activate();
			}
		}

	}

	public static void counter() {
		for (Effect e : effects.get("Counter")) {
			Counter k = (Counter) e;
			if (k.check()) {
				k.activate();
			}
		}
	}

	public static void encore(Card c) {
		for (Effect e : effects.get("Encore")) {
			Encore k = (Encore) e;
			if (k.check(c)) {
				k.activate();
			}
		}

	}

	public static void fieldToWaitingRoom(Card c) {
		for (Effect e : effects.get("FieldToWaitingRoom")) {
			FieldToWaitingRoom k = (FieldToWaitingRoom) e;
			if (k.check(c)) {
				k.activate();
			}
		}

	}

	public static void handToStage(Card c) {
		for (Effect e : effects.get("HandToStage")) {
			HandToStage k = (HandToStage) e;
			if (k.check(c)) {
				k.activate();
			}
		}
	}

	public static void handToWaitingRoom(Card c) {
		for (Effect e : effects.get("HandToWaitingRoom")) {
			HandToWaitingRoom k = (HandToWaitingRoom) e;
			if (k.check(c)) {
				k.activate();
			}
		}
	}

	public static void whenAttack(Card c) {
		for (Effect e : effects.get("WhenAttack")) {
			WhenAttack k = (WhenAttack) e;
			if (k.check(c)) {
				k.activate();
			}
		}
	}

	public static void whenReversed(Card c) {
		for (Effect e : effects.get("WhenReversed")) {
			WhenReversed k = (WhenReversed) e;
			if (k.check(c)) {
				k.activate();
			}
		}
	}

	public static void autoAbilityActivated(Effect c) {
		for (Effect e : effects.get("AutoAbilityActivated")) {
			AutoAbilityActivated k = (AutoAbilityActivated) e;
			if (k.check(c)) {
				k.activate();
			}
		}
	}

	public static void characterRested(Card c) {
		for (Effect e : effects.get("CharacterRested")) {
			CharacterRested k = (CharacterRested) e;
			if (k.check(c)) {
				k.activate();
			}
		}
	}

	public static void actAbilityActivated(Effect c) {
		for (Effect e : effects.get("ActAbilityActivated")) {
			ActAbilityActivated k = (ActAbilityActivated) e;
			if (k.check(c)) {
				k.activate();
			}
		}
	}

	public static void clockToWaitingRoom(Card c) {
		for (Effect e : effects.get("ClockToWaitingRoom")) {
			ClockToWaitingRoom k = (ClockToWaitingRoom) e;
			if (k.check(c)) {
				k.activate();
			}
		}
	}

	public static void cardDrawn(int i) {
		for (Effect e : effects.get("CardDrawn")) {
			CardDrawn k = (CardDrawn) e;
			if (k.check(i)) {
				k.activate();
			}
		}
	}

	public static void levelUp(Player c) {
		for (Effect e : effects.get("LevelUp")) {
			LevelUp k = (LevelUp) e;
			if (k.check(c)) {
				k.activate();
			}
		}
	}

	public static void waitingRoomToField(Card c) {
		for (Effect e : effects.get("WaitingRoomToField")) {
			WaitingRoomToField k = (WaitingRoomToField) e;
			if (k.check(c)) {
				k.activate();
			}
		}
	}

	public static void fieldToHand(Card c) {
		for (Effect e : effects.get("FieldToHand")) {
			FieldToHand k = (FieldToHand) e;
			if (k.check(c)) {
				k.activate();
			}
		}
	}

}
