package server;

import java.util.Random;
import java.util.Vector;

import client.Main;
import client.Prompt;
import exceptions.UnableToAtack;
import items.cards.Card;
import items.cards.Climax;
import items.field.Player;
import items.field.Stage;
import items.cards.Character;

public class Game {
	private Player p1;
	private Player p2;
	private Player active;
	private int attackType = -1;
	private int phase;
	private int turnCount = 0;
	private Character attacker = null;
	private Character attacked = null;
	public final static int DRAW = 0;
	public final static int STAND = 1;
	public final static int CLOCK = 2;
	public final static int MAIN = 3;
	public final static int CLIMAX = 4;
	public final static int BATTLE = 5;
	public final static int ENCORE = 6;
	public final static int FINAL = 7;
	public final static int ATACK = 15;
	public final static int TRIGGER = 25;
	public final static int COUNTER = 35;
	public final static int DAMAGE = 45;
	public final static int FIGHT = 55;
	public final static int FRONT = 0;
	public final static int SIDE = 1;
	public final static int DIRECT = -1;

	/**
	 * returns the attackType
	 * 
	 * @return
	 */
	public int getAttackType() {
		return attackType;
	}

	public Game(Player p1, Player p2) {
		this.p1 = p1;
		this.p2 = p2;
		phase = 0;
	}

	/**
	 * Returns the character attacking. ONLY VALID DURING BATTLE PHASE. May or
	 * may not contain non null crap during other phases.
	 * 
	 * @return A Character, not a Card
	 */
	public Character getAttacker() {
		return attacker;
	}

	/**
	 * Returns the character getting attacked. ONLY VALID DURING BATTLE PHASE.
	 * May or may not contain non null crap during other phases.
	 * 
	 * @return A Character, not a Card
	 */
	public Character getAttacked() {
		return attacked;
	}

	/**
	 * Returns the turn count.
	 * 
	 * @return ...
	 */
	public int turn() {
		return turnCount;
	}

	/**
	 * Returns the turn phase in numeric value. Compare with static values to
	 * get the actual phase.
	 * 
	 * @return ...
	 */
	public int getPhase() {
		return phase;
	}

	/**
	 * Returns the other player involved in the game.
	 * 
	 * @param p
	 *            The player whose opponent you want to determine.
	 * @return A Player, identifying the other player
	 */
	public Player opponent(Player p) {
		return p1 == p ? p2 : p1;
	}

	/**
	 * Determines the player who goes first.
	 */
	public void initialize() {
		Random rn = new Random();
		int i = rn.nextInt(2);
		active = i == 0 ? p1 : p2;
	}

	/**
	 * Draw phase. Draws a card for the active player. Then advances phase.
	 * 
	 * @handlers Draw Phase
	 */
	public void drawPhase() {
		EventHandler.drawPhase();
		active.drawCards(1);
		phase++;
	}

	/**
	 * Stand Phase. Sets all characters to Stand, then calls the handler. Then
	 * advances phase.
	 * 
	 * @handlers Stand Phase
	 */
	public void standPhase() {
		active.standAll();
		EventHandler.standPhase();
		phase++;

	}

	/**
	 * Clock Phase. Asks the active player if they want to drop a card to draw
	 * 2, then does so. Then advances phase.
	 * 
	 * @handlers Clock Phase
	 */
	public void clockPhase() {
		EventHandler.clockPhase();
		if (active.searchHand().length != 0
				&& Prompt.willing(
						"Do you want to put a card to clock and draw 2 cards?",
						active)) {
			Card c = Prompt.pick(active.searchHand(), 1, active);
			active.handToClock(c);
			active.drawCards(2);
		}
		phase++;
	}

	/**
	 * Conducts the main phase. Pretty much nothing is done here.
	 * 
	 * @handlers Main Phase
	 */
	public void mainPhase() {
		EventHandler.mainPhase();
		Prompt.mainPhase(active);
		phase++;
	}

	/**
	 * Climax Phase. Asks the player if they want to play a climax. Then does
	 * so, and advances phase.
	 * 
	 * @handlers Climax Phase
	 */
	public void climaxPhase() {
		EventHandler.climaxPhase();
		Vector<Card> v = new Vector<Card>();
		for (Card c : active.searchHand()) {
			if (c instanceof Climax)
				v.add(c);
		}
		if (v.size() > 0 && EventHandler.climaxAble()
				&& Prompt.willing("Do you want to play a Climax?", active)) {
			Climax c = (Climax) Prompt.pick((Card[]) v.toArray(), 1, active);
			active.playClimax(c);
		}
		phase++;
	}

	/**
	 * Battle phase. Start of the hostilities.
	 * 
	 * @handlers Battle Phase
	 */
	public void battlePhase() {
		EventHandler.battlePhase();
		Vector<Card> v = new Vector<Card>();
		for (Card c : active.searchStage())
			if (((Character) c).getSlot() < Stage.BACK_RIGHT
					&& c.getState() == Card.STAND)
				v.add(c);

		if (v.size() > 0) {
			boolean wants;
			do {
				wants = Prompt.willing("Do you want to atack?", active);
				if (wants) {
					phase += 10;
					Card k = Prompt.pick((Card[]) v.toArray(), 1, active);
					atack(k);
					v.remove(k);
				}
			} while (wants && v.size() < 0 && turnCount != 0);
		}
		phase++;
	}

	private void atack(Card c) {
		try {
			attackType = EventHandler.atackType();
			phase += 10;
			c.setState(Card.REST);
			attacker = (Character) c;
			attacked = (Character) opponent(active).searchStageBySlot(
					attacker.getSlot());
			switch (attackType) {
			case FRONT:
				frontAttack();
				break;
			case SIDE:
				sideAttack();
				break;
			case DIRECT:
				directAttack();
				break;
			}
		} catch (UnableToAtack e) {
			// TODO: handle exception
		}
	}

	private void frontAttack() {
		EventHandler.frontAttack(attacker);
		phase += 10;
		attacker.getOwner().revealTrigger();
		EventHandler.trigger(attacker.getOwner());
		attacker.getOwner().closeTrigger();
		phase += 10;
		EventHandler.counter();
		phase += 10;
		EventHandler.damageStep();
		opponent(active).takeDamage(attacker.getSoul());
		phase += 10;
		boolean mine = false;
		boolean yours = false;
		if (attacker.getPower() >= attacked.getPower()) {
			attacked.setState(Card.REVERSE);
			yours = true;
		}
		if (attacker.getPower() <= attacked.getPower()) {
			attacker.setState(Card.REVERSE);
			mine = true;
		}
		if (mine)
			EventHandler.reversed(attacker);
		if (yours)
			EventHandler.reversed(attacked);
		phase = BATTLE;
	}

	private void sideAttack() {
		EventHandler.sideAttack(attacker);
		phase += 10;
		attacker.getOwner().revealTrigger();
		EventHandler.trigger(attacker.getOwner());
		attacker.getOwner().closeTrigger();
		phase = DAMAGE;
		opponent(active).takeDamage(attacker.getSoul() - attacked.getLevel());
		phase = BATTLE;
	}

	private void directAttack() {
		EventHandler.directAttack(attacker);
		phase += 10;
		attacker.getOwner().revealTrigger();
		EventHandler.trigger(attacker.getOwner());
		attacker.getOwner().closeTrigger();
		phase = DAMAGE;
		opponent(active).takeDamage(attacker.getSoul() + 1);
		phase = BATTLE;
	}

	private void encoreStep() {
		EventHandler.encoreStep();
		for (Card c : active.searchStage()) {
			if ( c.getState() == Card.REVERSE) {
				active.stageToWR(c);
			}
		}
		for (Card c : opponent(active).searchStage()) {
			if (c.getState() == Card.REVERSE) {
				c.getOwner().stageToWR(c);
			}
		}
		phase++;
	}

	private void endPhase() {
		EventHandler.endPhase();
		active.closeClimax();
		active = opponent(active);
		phase = DRAW;
	}
}
