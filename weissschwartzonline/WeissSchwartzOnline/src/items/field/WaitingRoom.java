package items.field;

import items.cards.Card;

import java.util.Vector;

import exceptions.NoSuchCard;

public class WaitingRoom {
	private Vector<Card> waitingRoom = new Vector<Card>();

	/**
	 * Adds the given card to the Waiting Room
	 * 
	 * @param c
	 *            The card to add.
	 */
	public void add(Card c) {
		waitingRoom.add(c);
	}

	/**
	 * Retrieves, and deletes, the given card from the waiting room.
	 * 
	 * @param c
	 *            The card to retrieve
	 * @return The card c.
	 * @throws NoSuchCard
	 *             If there was no such card in the Waiting Room to begin with
	 */
	public Card pull(Card c) throws NoSuchCard {
		if (waitingRoom.remove(c))
			return c;
		throw new NoSuchCard();
	}

	/**
	 * Returns the whole WR as a vector.
	 * 
	 * @return ...
	 */
	public Vector<Card> getWaitingRoom() {
		return waitingRoom;
	}

	/**
	 * Returns the amount of cards in the WR
	 * 
	 * @return ...
	 */
	public int cardsInWaiting() {
		return waitingRoom.size();
	}
}
