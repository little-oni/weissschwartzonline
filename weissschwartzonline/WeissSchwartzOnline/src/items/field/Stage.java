package items.field;

import exceptions.NoSuchCard;
import exceptions.Replace;
import items.cards.Card;

public class Stage {
	public final static int FRONT_RIGHT = 0;
	public final static int FRONT_LEFT = 2;
	public final static int FRONT_CENTER = 1;
	public final static int BACK_RIGHT = 3;
	public final static int BACK_LEFT = 4;
	private Card[] stage = new Card[5];

	/**
	 * Places the Card c in the slot i iif i is empty.
	 * 
	 * @param c
	 *            The card to place
	 * @param i
	 *            The slot to place
	 * @throws Replace
	 *             If the slot is not empty
	 */
	public void place(Card c, int i) throws Replace {
		if (stage[i] == null)
			stage[i] = c;
		else
			throw new Replace();
	}

	/**
	 * Returns the instance of the Card c and deletes it.
	 * 
	 * @param c
	 *            The card to retrieve
	 * @return
	 * @throws NoSuchCard
	 *             If the card is not on the field.
	 */
	public Card pull(Card c) throws NoSuchCard {
		for (int i = 0; i < stage.length; i++) {
			if (stage[i] == c) {
				stage[i] = null;
				return c;
			}
		}
		throw new NoSuchCard();
	}

	/**
	 * Retrieves and destroys the card on the slot i USE WITH CARE.
	 * 
	 * @param i
	 * @return
	 */
	public Card pull(int i) {
		Card c = stage[i];
		stage[i] = null;
		return c;
	}

	/**
	 * Returns the whole thing. DEV ONLY
	 * 
	 * @return
	 */
	public Card[] getStage() {
		return stage;
	}

	/**
	 * Returns true if the Card c is on the field.
	 * 
	 * @param c
	 * @return
	 */
	public boolean contains(Card c) {
		int i;
		for (i = 0; i < stage.length && stage[i] != c; i++) {
		}
		return i != 5;
	}

	/**
	 * Returns the Card in the slot i. Null if nothing is there
	 * 
	 * @param i
	 * @return
	 */
	public Card peek(int i) {
		return stage[i];
	}
}
