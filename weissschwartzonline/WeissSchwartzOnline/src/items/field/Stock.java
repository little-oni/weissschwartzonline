package items.field;

import items.cards.Card;

import java.util.Stack;
import java.util.Vector;

import exceptions.NoSuchCard;

public class Stock {
	private Stack<Card> stock = new Stack<Card>();

	/**
	 * Adds the given card to the stock
	 * 
	 * @param c
	 *            The card to be added
	 */
	public void add(Card c) {
		stock.add(c);
	}

	/**
	 * Returns and deletes the first card from the stock.
	 * 
	 * @return ...
	 */
	public Card pull() {
		return stock.pop();
	}

	/**
	 * Returns and deletes the given card from the stock.
	 * 
	 * @param c
	 *            The card to be retrieved
	 * @return The card c.
	 * @throws NoSuchCard
	 *             If there was no such card at the stock to begin with.
	 */
	public Card pull(Card c) throws NoSuchCard {
		if (stock.remove(c))
			return c;
		throw new NoSuchCard();
	}

	/**
	 * Returns the amount of cards in stock.
	 * 
	 * @return ...
	 */
	public int cardsInStock() {
		return stock.size();
	}

	/**
	 * Return a vector holding all cards in stock.
	 * 
	 * @return ...
	 */
	public Vector<Card> getStock() {
		return stock;
	}
}
