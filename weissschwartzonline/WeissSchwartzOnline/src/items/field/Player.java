package items.field;

import java.util.Collections;
import java.util.EmptyStackException;
import java.util.List;
import java.util.Vector;

import client.Prompt;
import server.EventHandler;
import exceptions.GameLost;
import exceptions.LevelUp;
import exceptions.NoSuchCard;
import exceptions.Replace;
import items.cards.Card;
import items.cards.Climax;
import items.cards.effectInterface.Effect;
import items.cards.Character;

public class Player {
	private Hand hand = new Hand();
	private Clock clock = new Clock();
	private Deck deck = new Deck();
	private Level level = new Level();
	private Memory memory = new Memory();
	private Resolution res = new Resolution();
	private Stage stage = new Stage();
	private Stock stock = new Stock();
	private WaitingRoom waitingRoom = new WaitingRoom();
	private ClimaxSlot climaxSlot = new ClimaxSlot();

	/**
	 * Shuffles the waiting room in the deck. Does not call any handler.
	 */
	public void wrToDeck() {
		deck.getDeck().addAll(waitingRoom.getWaitingRoom());
		Collections.shuffle(deck.getDeck());
		waitingRoom = new WaitingRoom();
	}

	/**
	 * Heals the given card, throwing it to the waiting room. Crashes and exits
	 * if the card is not there.
	 * 
	 * @handlers Clock To Grave
	 * @param c
	 *            The card to heal.
	 */
	public void clockToWr(Card c) {
		try {
			waitingRoom.add(clock.pull(c));
		} catch (NoSuchCard e) {
			System.err.println("Card " + c
					+ " not found in clock due to some reason, terminate.");
			System.exit(-1);
		}
		EventHandler.clockToWaitingRoom(c);
	}

	/**
	 * Draws i cards. Resets deck and deals damage if the deck is emptied.
	 * 
	 * @handlers Cards Drawn
	 * @param i
	 *            The amount of cards to draw.
	 */
	public void drawCards(int i) {
		boolean reseted = false;
		for (int k = 0; k < i; k++) {
			try {
				hand.addCard(deck.pull());
			} catch (EmptyStackException e) {
				reseted = true;
				wrToDeck();
				hand.addCard(deck.pull());
			}
		}
		if (deck.cardsInDeck() == 0) {
			reseted = true;
			wrToDeck();
		}
		if (reseted)
			deckToClock();
		EventHandler.cardDrawn(i);
	}

	/**
	 * Puts the first card from deck to clock. Levels up if necessary. Calls no
	 * handlers ( but level up does )
	 */
	public void deckToClock() {
		try {
			clock.add(deck.pull());
		} catch (LevelUp e) {
			levelUp();
		}
	}

	/**
	 * Prompts the player to select a card from clock and level up with it. If
	 * the card is not found or the player reaches lvl4, the program ends.
	 * Throws all the cards from clock to the Waiting Room.
	 * 
	 * @handlers Level Up
	 */
	public void levelUp() {
		try {
			level.add(clock.pull(Prompt.levelUp(this)));
		} catch (GameLost | NoSuchCard e) {
			System.out.println("game lost");
			System.exit(1);
		}
		for (Card c : clock.getClock()) {
			try {
				waitingRoom.add(clock.pull(c));
			} catch (NoSuchCard e) {
				e.printStackTrace();
			}
		}
		EventHandler.levelUp(this);
	}

	/**
	 * Places the card c from hand to the stage in the slot i. Prompts the
	 * player to replace if necessary.
	 * 
	 * @handlers Field To Waiting Room if there was a replacement
	 * @handlers Hand To Field
	 * @param c
	 *            The card to be placed
	 * @param i
	 *            The slot where to place. Can be selected via Stage.(static
	 *            int)
	 */
	public void handToStage(Card c, int i) {
		try {
			stage.place(hand.pull(c), i);
		} catch (Replace e) {
			if (Prompt.replace(this)) {
				try {
					Card c1 = stage.pull(i);
					waitingRoom.add(c1);
					EventHandler.fieldToWaitingRoom(c1);
					stage.place(c, i);
				} catch (Exception e1) {
				}
			} else {
				hand.addCard(c);
				return;
			}
		} catch (NoSuchCard e) {
		}
		EventHandler.handToStage(c);
	}

	/**
	 * Places the card c from the waiting room to stage in the slot i. Prompts
	 * the player to replace if necessary.
	 * 
	 * @handlers Card From Field to Grave if there was a card replaced
	 * @handlers Card From Grave to Field
	 * @param c
	 *            The card to be placed
	 * @param i
	 *            The slot to be placed in
	 */
	public void waitingRoomToStage(Card c, int i) {
		try {
			stage.place(waitingRoom.pull(c), i);
		} catch (Replace e) {
			if (Prompt.replace(this)) {
				try {
					Card c1 = stage.pull(i);
					waitingRoom.add(c1);
					EventHandler.fieldToWaitingRoom(c1);
					stage.place(c, i);
				} catch (Exception e1) {
				}
			} else {
				waitingRoom.add(c);
				return;
			}
		} catch (NoSuchCard e) {
		}
		EventHandler.waitingRoomToField(c);
	}

	/**
	 * Exiles the card c, placing it to memory
	 * 
	 * @handlers Exile
	 * @param c
	 *            The card to be exiled
	 */
	public void cardToMemory(Card c) {
		memory.add(c);
	}

	/**
	 * Throws the card c from hand to waiting room.
	 * 
	 * @handler Card From Hand to Waiting Room
	 * @param c
	 *            The card to be thrown
	 */
	public void handToWaitingRoom(Card c) {
		try {
			waitingRoom.add(hand.pull(c));
			EventHandler.handToWaitingRoom(c);
		} catch (Exception e) {
		}
	}

	/**
	 * Places the first card of the deck into the resolution area. Does not
	 * trigger anything, but does reset deck if needed.
	 */
	public void revealTrigger() {
		res.add(deck.pull());
		if (deck.cardsInDeck() == 0) {
			wrToDeck();
			deckToClock();
		}
	}

	/**
	 * Places the first card from the resolution area to the stock.
	 */
	public void closeTrigger() {
		stock.add(res.pull());
	}

	/**
	 * Places the first i cards from the deck into the resolution area. Resets
	 * the deck if needed. Does not trigger anything.
	 * 
	 * @param i
	 *            The amount of cards to brainstorm.
	 */
	public void brainstormOpen(int i) {
		boolean reseted = false;
		for (int k = 0; k < i; k++) {
			try {
				res.add(deck.pull());
			} catch (Exception e) {
				wrToDeck();
				reseted = true;
				res.add(deck.pull());
			}
		}
		if (deck.cardsInDeck() == 0) {
			wrToDeck();
			reseted = true;
		}
		if (reseted)
			deckToClock();
	}

	/**
	 * Empties the resolution area into the waiting room.
	 * 
	 * @handlers Card to Grave
	 */
	public void brainstormClose() {
		for (Card c : res.getRes()) {
			try {
				waitingRoom.add(res.pull(c));
			} catch (NoSuchCard e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Returns the given card from stage to hand.
	 * 
	 * @param c
	 *            The card to return
	 * @handlers Card from Field to Hand
	 */
	public void stageToHand(Card c) {
		try {
			hand.addCard(stage.pull(c));
			EventHandler.fieldToHand(c);
		} catch (NoSuchCard e) {
			e.printStackTrace();
			Prompt.warning("Well, fuck", this);
			System.exit(1);
		}
	}

	/**
	 * Places up to i cards from the deck into the clock, going through the
	 * resolution area. If a climax is revealed this way, the damage is canceled
	 * and placed to the grave. If the deck is emptied, is reseted. If the
	 * damage is not canceled and there are seven or more cards in clock, a
	 * level up is performed (triggers level up). If the damage is not
	 * cancelable, don't use this method, use deckToClock instead
	 * 
	 * @handlers Damage Canceled if the damage is canceled.
	 * @handlers Damage Taken if the damage is not canceled.
	 * @param i
	 *            The amount of damage to taken.
	 */
	public void takeDamage(int i) {
		boolean canceled = false;
		boolean reseted = false;
		for (int k = 0; k < i && !canceled; k++) {
			try {
				res.add(deck.pull());

			} catch (EmptyStackException e) {
				wrToDeck();
				reseted = true;
				res.add(deck.pull());
			}
			if (res.peek() instanceof Climax) {
				canceled = true;
			}
		}
		if (canceled) {
			for (Card c : res.getRes()) {
				try {
					waitingRoom.add(res.pull(c));
				} catch (NoSuchCard e) {
					e.printStackTrace();
				}
			}
			EventHandler.damageCanceled();
		} else {
			for (Card c : res.getRes()) {
				try {
					clock.add(res.pull(c));
				} catch (LevelUp e) {
					levelUp();
					try {
						clock.add(c);
					} catch (LevelUp e1) {
						e1.printStackTrace();
					}
				} catch (NoSuchCard e) {
					e.printStackTrace();
				}
			}
			EventHandler.damageTaken();
		}
		if (deck.cardsInDeck() == 0) {
			wrToDeck();
			reseted = true;
		}
		if (reseted) {
			deckToClock();
		}
	}

	/**
	 * Places the last i cards from stock to waiting room. If there is any stock
	 * replacement effects active, they would activate here without asking.
	 * 
	 * @handlers Cost Paid
	 * @param i
	 *            The cost to pay.
	 * @param eff
	 *            The effect that is getting paid. Use "this" when calling from
	 *            an effect.
	 */
	public void pay(int i, Effect eff) {
		for (int k = 0; k < i; k++) {
			if (EventHandler.canReplace(eff) > 0) {
				EventHandler.replace(eff);
			} else {
				waitingRoom.add(stock.pull());
			}
		}
		EventHandler.costPaid(i);
	}

	/**
	 * Checks whether there is or not stock to pay. If there are any replacement
	 * effects active, they will be taken into account.
	 * 
	 * @param i
	 *            The cost to pay.
	 * @param eff
	 *            The effect getting paid.
	 * @return True if there is enough stock, false IOC.
	 */
	public boolean hasStock(int i, Effect eff) {
		return EventHandler.canReplace(eff) + stock.cardsInStock() >= i;
	}

	/**
	 * Searches the waiting room for any card that matches the given name.
	 * Returns an array holding all cards that match. This search is performed
	 * on the exact name only.
	 * 
	 * @param string
	 *            The name of the card being searched.
	 * @return An array holding all the cards that match at least one name with
	 *         the name being searched.
	 * @throws NoSuchCard
	 *             If there is no matching card in the waiting room.
	 */
	public Card[] searchWaitingByName(String string) throws NoSuchCard {
		Vector<Card> v = new Vector<Card>();
		List<Card> wr = waitingRoom.getWaitingRoom();
		for (Card c : wr) {
			if (c.getNames().contains(string))
				v.add(c);
		}
		if (v.size() == 0)
			throw new NoSuchCard();
		return (Card[]) v.toArray();
	}

	/**
	 * Adds the given card from waiting room to hand.
	 * 
	 * @handlers Card From Grave to Hand
	 * @param c
	 *            The card to add
	 */
	public void waitingRoomToHand(Card c) {
		try {
			c.getOwner().hand.addCard(c.getOwner().waitingRoom.pull(c));
			EventHandler.cardFromGraveToHand(c);
		} catch (NoSuchCard e) {
			System.err.println("Something went very wrong");
			System.exit(-1);
		}
	}

	/**
	 * Searches the stage for any card that matches the given trait. Returns an
	 * array holding all cards that match.
	 * 
	 * @param string
	 *            The traits of the card being searched.
	 * @return An array holding all the cards that match at least one trait with
	 *         the one being searched.
	 * @throws NoSuchCard
	 *             If there is no matching card on stage.
	 */
	public Character[] searchStageByTrait(String string) throws NoSuchCard {
		Vector<Card> v = new Vector<Card>();
		for (int i = 0; i < stage.getStage().length; i++) {
			if (stage.getStage()[i] != null
					&& ((Character) stage.getStage()[i]).getTraits().contains(
							string)) {
				v.add(stage.getStage()[i]);
			}
		}
		if (v.size() == 0)
			throw new NoSuchCard();
		return (Character[]) v.toArray();
	}

	/**
	 * Returns an array holding all characters in stage, no nulls. The
	 * characters will be in slot order, and empty slots will be ommited.
	 * 
	 * @return ...
	 */
	public Card[] searchStage() {
		Vector<Card> res = new Vector<Card>();
		for (int i = 0; i < stage.getStage().length; i++) {
			if (stage.getStage()[i] != null) {
				res.add(stage.getStage()[i]);
			}
		}
		return (Card[]) res.toArray();
	}

	/**
	 * Returns an array holding all cards in hand.
	 * 
	 * @return ...
	 */
	public Card[] searchHand() {
		return (Card[]) hand.getHand().toArray();
	}

	/**
	 * Returns whatever there is in that slot, nulls included. Use with
	 * Card.(SLOT) abstracts to get cards in given positions.
	 * 
	 * @param slot
	 *            The slot we want to check
	 * @return The card in the slot, if any.
	 */
	public Card searchStageBySlot(int slot) {
		return stage.getStage()[slot];
	}

	/**
	 * Stands all characters on stage.
	 */
	public void standAll() {
		for (int i = 0; i < stage.getStage().length; i++) {
			if (stage.getStage()[i] != null
					&& stage.getStage()[i].getState() == Card.REST) {
				stage.getStage()[i].setState(Card.STAND);
			}
		}
	}

	/**
	 * Throws a card from hand to clock.
	 * 
	 * @param c
	 *            The card to throw.
	 * @handlers Card from Hand to Clock.
	 */
	public void handToClock(Card c) {
		try {
			clock.add(hand.pull(c));
			EventHandler.cardFromHandToClock(c);
		} catch (LevelUp e) {
			levelUp();
			e.printStackTrace();
		} catch (NoSuchCard e) {
			e.printStackTrace();
			Prompt.warning("Well, fuck", this);
		}

	}

	/**
	 * Places the climax from hand to stage.
	 * 
	 * @param c
	 *            The climax to activate
	 * @handlers Climax Played
	 */
	public void playClimax(Climax c) {
		try {
			climaxSlot.add(hand.pull(c));
		} catch (NoSuchCard e) {
			e.printStackTrace();
			Prompt.warning("Well, fuck", this);
			System.exit(1);
		}
		EventHandler.climaxPlayed(c);
	}

	/**
	 * Places the card from stage to the waiting room.
	 * 
	 * @param c
	 *            The card to place
	 * @handlers Card From Field to Grave
	 */
	public void stageToWR(Card c) {
		try {
			waitingRoom.add(stage.pull(c));
			EventHandler.fieldToWaitingRoom(c);
		} catch (NoSuchCard e) {
			e.printStackTrace();
			Prompt.warning("Well, fuck", this);
			System.exit(1);
		}
	}

	/**
	 * Returns the active climax if any. Null if none.
	 * 
	 * @return ...
	 */
	public Card activeClimax() {
		return climaxSlot.peek();
	}

	/**
	 * Cleans the climax slot, does nothing if it was empty.
	 */
	public void closeClimax() {
		if (climaxSlot.peek() != null)
			waitingRoom.add(climaxSlot.pull());

	}

}
