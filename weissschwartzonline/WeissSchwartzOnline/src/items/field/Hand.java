package items.field;

import items.cards.Card;

import java.util.Vector;

import exceptions.NoSuchCard;

public class Hand {
	private Vector<Card> hand = new Vector<Card>();

	/**
	 * Adds the given card to the hand.
	 * 
	 * @param c
	 *            The card to add
	 */
	public void addCard(Card c) {
		hand.add(c);
	}

	/**
	 * Removes and returns the given card from the hand.
	 * 
	 * @param c
	 *            The card to remove.
	 * @return The card passed as parameter (allows to find, pull, use all in
	 *         succession, without first storing the card in a variable)
	 * @throws NoSuchCard
	 *             If there was no such card in the hand to begin with.
	 */
	public Card pull(Card c) throws NoSuchCard {
		if (hand.remove(c))
			return c;
		throw new NoSuchCard();
	}

	/**
	 * Returns the amount of cards in hand.
	 * 
	 * @return ...
	 */
	public int cardsInHand() {
		return hand.size();
	}

	/**
	 * Returns a vector holding all the cards in hand.
	 * 
	 * @return ...
	 */
	public Vector<Card> getHand() {
		return hand;
	}
}
