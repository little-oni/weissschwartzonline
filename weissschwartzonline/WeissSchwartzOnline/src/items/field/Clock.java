package items.field;

import java.util.Vector;

import ownTAD.LimitedStack;
import client.Prompt;
import exceptions.LevelUp;
import exceptions.NoSuchCard;
import items.cards.Card;

public class Clock {
	private LimitedStack<Card> clock = new LimitedStack<Card>(7);

	public void add(Card c) throws LevelUp {
		if (!(clock.push(c) == null))
			throw new LevelUp();
	}

	public Card pull() {
		return clock.pop();
	}

	public Card pull(Card c) throws NoSuchCard {
		if (clock.remove(c))
			return c;
		throw new NoSuchCard();
	}

	public Vector<Card> getClock() {
		return clock;
	}

	public int cardsInClock() {
		return clock.size();
	}

	public boolean colorPresence(int i) {
		return true;
	}
}
