package items.field;

import items.cards.Card;

import java.util.Stack;
import java.util.Vector;

import exceptions.NoSuchCard;

public class Deck {
	private Stack<Card> deck = new Stack<Card>();

	/**
	 * Adds the given card on top of the deck
	 * 
	 * @param c
	 *            The given card
	 */
	public void add(Card c) {
		deck.add(c);
	}

	/**
	 * Adds the given card on the bottom of the deck
	 * 
	 * @param c
	 *            The given card
	 */
	public void addLast(Card c) {
		deck.add(0, c);
	}

	/**
	 * Returns and deletes the card on top of the deck.
	 * 
	 * @return ...
	 */
	public Card pull() {
		return deck.pop();
	}

	/**
	 * Returns and deletes the given card from the deck
	 * 
	 * @param c
	 *            The given card
	 * @return The given card
	 * @throws NoSuchCard
	 *             If there was no such card in the deck to begin with
	 */
	public Card pull(Card c) throws NoSuchCard {
		if (deck.remove(c))
			return c;
		throw new NoSuchCard();
	}

	/**
	 * Returns a vector holding all cards in deck.
	 * 
	 * @return ...
	 */
	public Vector<Card> getDeck() {
		return deck;
	}

	/**
	 * Returns the amount of cards in deck
	 * 
	 * @return ...
	 */
	public int cardsInDeck() {
		return deck.size();
	}
}
