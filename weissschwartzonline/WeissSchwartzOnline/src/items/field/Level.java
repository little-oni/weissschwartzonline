package items.field;

import java.util.Vector;

import exceptions.GameLost;
import exceptions.NoSuchCard;
import items.cards.Card;
import ownTAD.LimitedStack;

public class Level {
	private LimitedStack<Card> level = new LimitedStack<Card>(3);

	public void add(Card c) throws GameLost {
		if (level.push(c) == null) {
			throw new GameLost();
		}
	}
	public Card pull(Card c) throws NoSuchCard{
		if(level.remove(c))
			return c;
		throw new NoSuchCard();
	}
	public Vector<Card> getLevl(){
		return level;
	}
	public int cardsInLevel(){
		return level.size();
	}
	public boolean colorPresence(int i){
		return true;
	}
}
