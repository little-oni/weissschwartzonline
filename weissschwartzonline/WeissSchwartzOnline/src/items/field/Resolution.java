package items.field;

import items.cards.Card;

import java.util.Stack;
import java.util.Vector;

import exceptions.NoSuchCard;

public class Resolution {
	private Stack<Card> resolution = new Stack<Card>();

	public void add(Card c) {
		resolution.push(c);
	}

	public Card pull() {
		return resolution.pop();
	}

	public Card pull(Card c) throws NoSuchCard {
		if (resolution.remove(c))
			return c;
		throw new NoSuchCard();
	}

	public Card peek() {
		return resolution.peek();
	}

	public Vector<Card> getRes() {
		return resolution;
	}

	public int cardsLingering() {
		return resolution.size();
	}
}
