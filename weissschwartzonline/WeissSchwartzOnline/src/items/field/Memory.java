package items.field;

import items.cards.Card;

import java.util.Vector;

import exceptions.NoSuchCard;

public class Memory {
	private Vector<Card> memory = new Vector<Card>();

	/**
	 * Adds the given card to memory
	 * 
	 * @param c
	 *            The given card
	 */
	public void add(Card c) {
		memory.add(c);
	}

	/**
	 * Returns and removes the given card from memory
	 * 
	 * @param c
	 *            The given card
	 * @return The given card
	 * @throws NoSuchCard
	 *             If there was no such card in memory to begin with
	 */
	public Card pull(Card c) throws NoSuchCard {
		if (memory.remove(c))
			return c;
		throw new NoSuchCard();
	}

	/**
	 * Returns a vector holding all cards in memory
	 * 
	 * @return ...
	 */
	public Vector<Card> getMemory() {
		return memory;
	}

	/**
	 * Returns the amount of cards in memory
	 * 
	 * @return ...
	 */
	public int cardsInMemory() {
		return memory.size();
	}
}
