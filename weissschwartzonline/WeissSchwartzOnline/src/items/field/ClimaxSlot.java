package items.field;

import items.cards.Card;

public class ClimaxSlot {
	private Card climax;

	public void add(Card c) {
		climax = c;
	}

	public Card pull() {
		Card c = climax;
		climax = null;
		return c;
	}
	public Card peek(){
		return climax;
	}
}
