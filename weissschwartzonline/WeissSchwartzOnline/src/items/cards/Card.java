package items.cards;

import items.field.Player;
import items.interfaces.I_Effect;
import items.interfaces.I_Trigger;

import java.util.Vector;

public abstract class Card {
	private Vector<String> names = new Vector<String>();
	private Vector<Integer> colors = new Vector<Integer>();
	private int level = 0;
	private I_Trigger trigger = null;
	private Vector<I_Effect> effects = null;
	private String id = "";
	private Player owner = null;
	private int state = 0;
	public static final int STAND = 0;
	public static final int REST = 1;
	public static final int REVERSE = 2;

	/**
	 * Sets the state of the card to the one given. Has no effect if the card is
	 * not a character.
	 * 
	 * @param i
	 *            The state to set. Use statics.
	 */
	public void setState(int i) {
		state = i;
	}

	/**
	 * Returns an integer symbolizing the state of the card. Compare to statics
	 * to identify.
	 * 
	 * @return ...
	 */
	public int getState() {
		return state;
	}

	/**
	 * Returns the player owning the card.
	 * 
	 * @return ...
	 */
	public Player getOwner() {
		return owner;
	}

	/**
	 * Launches the trigger method of the card. Not implemented yet
	 */
	public void trigger() {
		trigger.trigger();
	}

	/**
	 * Returns all the names of the card. Use .contains to identify.
	 * 
	 * @return ...
	 */
	public Vector<String> getNames() {
		return names;
	}

	/**
	 * Returns all the colors of the card. Use .contains to identify.
	 * 
	 * @return ...
	 */
	public Vector<Integer> getColors() {
		return colors;
	}

	/**
	 * Returns the level of the card. The climaxes are usually considered lv0
	 * 
	 * @return ...
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * Returns an array holding all effects. Has no use yet.
	 * 
	 * @return ...
	 */
	public Vector<I_Effect> getEffects() {
		return effects;
	}

	/**
	 * Returns the id of the card.
	 * 
	 * @return ...
	 */
	public String getId() {
		return id;
	}

	/**
	 * For console use only.
	 */
	public String toString() {
		return "[" + id + "]";
	}
}
