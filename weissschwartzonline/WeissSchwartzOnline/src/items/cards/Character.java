package items.cards;

import java.util.Vector;

public class Character extends Card {
	private Vector<String> traits = null;
	private int cost = 0;
	private int power = 0;
	private int soul = 0;
	private int bonusSoul = 0;
	private int slot = -1;

	/**
	 * Returns all the traits of the card. Use .contains to identify
	 * 
	 * @return ...
	 */
	public Vector<String> getTraits() {
		return traits;
	}

	/**
	 * Returns the cost of the card.
	 * 
	 * @return ...
	 */
	public int getCost() {
		return cost;
	}

	/**
	 * Returns the power of the card (effective power)
	 * 
	 * @return ...
	 */
	public int getPower() {
		return power;
	}

	/**
	 * Returns the soul of the card (effective soul)
	 * 
	 * @return ...
	 */
	public int getSoul() {
		return soul + bonusSoul >= 0 ? soul + bonusSoul : 0;
	}

	/**
	 * Sets the soul modifier, adding i to it.
	 * 
	 * @param i
	 *            The amount to add to the modifier. If you want to subtract,
	 *            make it negative.
	 */
	public void giveSoul(int i) {
		bonusSoul = bonusSoul + i;
	}

	/*
	 * Returns whether the character is on field. Not implemented yet.
	 */
	public boolean onField() {
		// TODO Auto-generated method stub
		return false;
	}

	public int getSlot() {
		return slot;
	}

	public void givePower(int i) {
		// TODO Auto-generated method stub

	}

	public void setSlot(int n) {
		slot = n;
	}
}
