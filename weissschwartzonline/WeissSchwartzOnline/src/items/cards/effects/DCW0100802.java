package items.cards.effects;

import server.EventHandler;
import server.Game;
import client.Main;
import client.Prompt;
import items.cards.Card;
import items.cards.effectInterface.WhenAttack;

public class DCW0100802 implements WhenAttack{
	private Card card;
	
	public void bind(Card c) {
		card = c;
	}

	
	public void payCost() {
	}

	
	public void activate() {
		Card[] v = card.getOwner().searchHand();
		Card c = Prompt.pick(v, 1, Main.game.opponent(card.getOwner()));
		Main.game.opponent(card.getOwner()).handToWaitingRoom(c);
		EventHandler.autoAbilityActivated(this);
	}

	
	public Card bound() {
		return card;
	}

	
	public boolean check(Card c) {
		return Main.game.getAttacked() == card && Main.game.getAttackType() == Game.FRONT;
	}

}
