package items.cards.effects;

import items.cards.Card;
import items.cards.effectInterface.Activated;
import items.cards.effectInterface.DispellOnEndTurn;

public class DCW0101002 implements Activated, DispellOnEndTurn{
	private Card card;

	public void bind(Card c) {
		card = c;
	}

	public void payCost() {
		card.getOwner().pay(1, this);
	}

	public void activate() {
		//TODO this gains one soul
	}

	public Card bound() {
		return card;
	}
	
	public boolean check() {
		return card.getOwner().hasStock(1, this);
	}
	
	public void dispell() {
		// TODO at End of the turn
		
	}
}
