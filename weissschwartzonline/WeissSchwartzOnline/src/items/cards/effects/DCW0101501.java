package items.cards.effects;

import items.cards.Card;
import items.cards.effectInterface.Counter;

public class DCW0101501 implements Counter{
	private Card card;
	
	public boolean check() {
		return false;
	}

	public void bind(Card c) {
		card=c;
	}

	public void payCost() {
		
	}

	public void activate() {
		
	}

	public Card bound() {
		return card;
	}

}
