package items.cards.effects;

import java.util.Vector;

import server.EventHandler;
import client.Prompt;
import items.cards.Card;
import items.cards.effectInterface.HandToStage;

public class DCW0100201 implements HandToStage {
	private Card card;

	@Override
	public void bind(Card c) {
		card = c;
	}

	public void payCost() {
	}

	public void activate() {
		Card[] v = card.getOwner().searchStage();
		Vector<Card> k = new Vector<Card>();
		for (Card c : v)
			if (c.getState() == Card.STAND)
				k.add(c);

		Card c = Prompt.pick((Card[]) k.toArray(), 1, card.getOwner());
		c.setState(Card.REST);
		EventHandler.characterRested(c);
		EventHandler.autoAbilityActivated(this);
	}

	@Override
	public Card bound() {
		return card;
	}

	@Override
	public boolean check(Card c) {
		return c == card;
	}

}
