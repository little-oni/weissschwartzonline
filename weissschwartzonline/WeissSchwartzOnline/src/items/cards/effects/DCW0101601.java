package items.cards.effects;

import server.EventHandler;
import client.Main;
import client.Prompt;
import items.cards.Card;
import items.cards.effectInterface.WhenReversed;
import items.cards.Character;

public class DCW0101601 implements WhenReversed {
	private Card card;

	public void bind(Card c) {
		card = c;
	}

	public void payCost() {

	}

	public void activate() {
		Character op = card == Main.game.getAttacker() ? Main.game
				.getAttacked() : Main.game.getAttacker();
		op.getOwner().stageToHand(op);
		EventHandler.autoAbilityActivated(this);
	}

	public Card bound() {
		return card;
	}

	public boolean check(Card c) {
		if (c == card) {
			Character op = card == Main.game.getAttacker() ? Main.game
					.getAttacked() : Main.game.getAttacker();
			return op.getLevel() <= 1
					&& Prompt.willing("Will you return " + op.getNames().get(0)
							+ " to hand?", card.getOwner());
		}
		return false;
	}

}
