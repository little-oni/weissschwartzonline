package items.cards.effects;

import server.EventHandler;
import client.Main;
import client.Prompt;
import items.cards.Card;
import items.cards.effectInterface.WhenAttack;

public class DCW0101001 implements WhenAttack {
	private Card card;

	public void bind(Card c) {
		card = c;

	}

	public void payCost() {
	}

	public void activate() {
		Card[] c = Main.game.opponent(card.getOwner()).searchStage();
		if (c.length == 0) {
			Prompt.warning("No legal targets", card.getOwner());
			return;
		}
		Card d = Prompt.pick(c, 1, card.getOwner());
		d.getOwner().stageToHand(d);
		EventHandler.autoAbilityActivated(this);
	}

	public Card bound() {
		return card;
	}

	public boolean check(Card c) {
		return card == c && card.getOwner().activeClimax().getNames().contains("Day of Nullified Existence")
				&& Prompt.willing("return a card to hand?", card.getOwner());
	}

}
