package items.cards.effects;

import client.Prompt;
import exceptions.NoSuchCard;
import server.EventHandler;
import items.cards.Card;
import items.cards.effectInterface.Bond;

public class DCW0100101 implements Bond {
	private Card card;

	public void bind(Card c) {
		card = c;

	}

	public void payCost() {
		card.getOwner().pay(1, this);

	}

	public void activate() {
		try {
			Card c = card.getOwner().searchWaitingByName("Yoshiyuki Sakurai")[0];
			card.getOwner().waitingRoomToHand(c);
		} catch (NoSuchCard e) {
			Prompt.warning("Card not Found", card.getOwner());
		}
		EventHandler.autoAbilityActivated(this);
	}

	public boolean check(Card c) {
		return c == card
				&& card.getOwner().hasStock(1, this)
				&& Prompt.willing("Activate " + card.getNames().get(0)
						+ "'s bond?", card.getOwner());
	}

	@Override
	public Card bound() {
		return card;
	}

}
