package items.cards.effects;

import java.util.Vector;

import server.EventHandler;
import client.Main;
import client.Prompt;
import items.cards.Card;
import items.cards.Character;
import items.cards.effectInterface.Activated;
import items.field.Stage;

public class DCW0101401 implements Activated{
	private Card card;
	
	public void bind(Card c) {
		card = c;
	}

	@Override
	public void payCost() {
		card.getOwner().pay(3, this);
	}

	@Override
	public void activate() {
		Card [] c = Main.game.opponent(card.getOwner()).searchStage();
		Vector<Card> d = new Vector<Card>();
		for (int i = 0; i < c.length; i++) {
			if (((Character) c[i]).getLevel() <= 1
					&& ((Character) c[i]).getSlot() < Stage.BACK_RIGHT) {
				d.add(c[i]);
			}
		}
		if (d.size() == 0) {
			Prompt.warning("No legal targets", card.getOwner());
			return;
		}
		Card[] e =(Card[]) d.toArray();
		Card f = Prompt.pick(e, 1, card.getOwner());
		f.getOwner().stageToHand(f);
		EventHandler.actAbilityActivated(this);
	}


	public Card bound() {
		return card;
	}

	@Override
	public boolean check() {
		return card.getOwner().hasStock(3, this);
	}

}
