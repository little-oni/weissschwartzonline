package items.cards.effects;

import java.util.Vector;

import server.EventHandler;
import client.Main;
import client.Prompt;
import items.cards.Card;
import items.cards.Character;
import items.cards.effectInterface.WhenAttack;
import items.field.Stage;

public class DCW0101102 implements WhenAttack {
	private Card card;

	public void bind(Card c) {
		card = c;

	}

	@Override
	public void payCost() {
	}

	public void activate() {
		Card[] c = Main.game.opponent(card.getOwner()).searchStage();
		Vector<Card> d = new Vector<Card>();
		for (int i = 0; i < c.length; i++) {
			if (((Character) c[i]).getSlot() < Stage.BACK_RIGHT) {
				d.add(c[i]);
			}
		}
		if (d.size() == 0) {
			Prompt.warning("No legal targets", card.getOwner());
			return;
		}
		Card[] e = (Card[]) d.toArray();
		Card f = Prompt.pick(e, 1, card.getOwner());
		f.getOwner().stageToHand(f);
		EventHandler.autoAbilityActivated(this);
	}

	@Override
	public Card bound() {
		return card;
	}

	@Override
	public boolean check(Card c) {
		return card == c
				&& card.getOwner().activeClimax().getNames()
						.contains("Dreams of Magicians")
				&& Prompt.willing("return a card to hand?", card.getOwner());
	}

}
