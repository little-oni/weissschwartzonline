package items.cards.effects;

import java.util.Vector;

import client.Prompt;
import server.EventHandler;
import items.cards.Card;
import items.cards.effectInterface.Encore;
import items.cards.Character;

public class DCW0100202 implements Encore {
	private Card card;

	@Override
	public boolean check(Card c) {
		boolean able = card == c && EventHandler.encoreAble();
		boolean stop = false;
		if (able) {
			Card[] v = card.getOwner().searchHand();

			for (int i = 0; i < v.length && !stop; i++) {
				stop = (v[i] instanceof Character);
			}
		}
		return able
				&& stop
				&& Prompt
						.willing(
								"Do you want to activate encore by discarding a character?",
								card.getOwner());
	}

	@Override
	public void bind(Card c) {
		card = c;
	}

	@Override
	public void payCost() {
		Card[] v = card.getOwner().searchHand();
		Vector<Card> b = new Vector<Card>();
		for (int i = 0; i < v.length; i++) {
			if (v[i] instanceof Character)
				b.add(v[i]);
		}
		Card[] c = (Card[]) b.toArray();
		Card d = Prompt.pick(c, 1, card.getOwner());
		card.getOwner().handToWaitingRoom(d);
	}

	@Override
	public void activate() {
		Character c = (Character) card;
		c.getOwner().waitingRoomToStage(card, c.getSlot());
		EventHandler.autoAbilityActivated(this);
		
	}

	@Override
	public Card bound() {
		return card;
	}

}
