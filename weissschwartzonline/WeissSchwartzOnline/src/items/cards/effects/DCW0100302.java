package items.cards.effects;

import server.EventHandler;
import items.cards.Card;
import items.cards.effectInterface.DispellOnEndTurn;
import items.cards.effectInterface.WhenAttack;
import items.cards.Character;

public class DCW0100302 implements WhenAttack, DispellOnEndTurn {
	private Card card;
	private Character target[] = new Character[5];

	public void bind(Card c) {
		card = c;
	}

	public void payCost() {

	}

	public void activate() {
		Card[] v = card.getOwner().searchStage();
		int k = 0;
		for (int i = 0; i < v.length; i++) {
			target[k] = (Character) v[i];
			target[k].givePower(500);
			k++;
		}
		EventHandler.autoAbilityActivated(this);
	}

	public Card bound() {
		return card;
	}

	public boolean check(Card c) {
		return card == c;
	}

	public void dispell() {
		for (int i = 0; i < target.length && target[i] != null; i++) {
			if (target[i].onField()) {
				target[i].givePower(-500);
				target[i] = null;
			}
		}
	}
}
