package items.cards.effects;

import java.util.Vector;

import server.EventHandler;
import client.Main;
import client.Prompt;
import items.cards.Card;
import items.cards.effectInterface.DispellOnEndTurn;
import items.cards.effectInterface.WhenReversed;
import items.cards.Character;

public class DCW0100502 implements WhenReversed, DispellOnEndTurn {
	private Card card;
	private Vector<Character> target = new Vector<Character>();

	public void bind(Card c) {
		card = c;
	}

	public void payCost() {
	}

	public void activate() {
		Card c = Prompt.pick(card.getOwner().searchStage(), 1, card.getOwner());
		Character k = (Character) c;
		k.givePower(1000);
		target.add(k);
		EventHandler.autoAbilityActivated(this);
	}

	public Card bound() {
		return card;
	}

	public boolean check(Card c) {
		return c.getOwner() == Main.game.opponent(card.getOwner());
	}

	public void dispell() {
		for (Character c : target) {
			if (c.onField()) {
				c.givePower(-1000);
				target.remove(c);
			}
		}
	}

}
