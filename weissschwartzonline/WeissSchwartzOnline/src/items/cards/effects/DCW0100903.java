package items.cards.effects;

import java.util.Vector;

import server.EventHandler;
import client.Main;
import client.Prompt;
import items.cards.Card;
import items.cards.effectInterface.Activated;
import items.cards.Character;
import items.field.Stage;

public class DCW0100903 implements Activated {
	private Card card;

	public void bind(Card c) {
		card = c;
	}

	public void payCost() {
		card.getOwner().pay(4, this);
	}

	public void activate() {
		Card[] c = Main.game.opponent(card.getOwner()).searchStage();
		Vector<Card> d = new Vector<Card>();
		for (int i = 0; i < c.length; i++) {
			if (((Character) c[i]).getLevel() <= 2
					&& ((Character) c[i]).getSlot() < Stage.BACK_RIGHT) {
				d.add(c[i]);
			}
		}
		if (d.size() == 0) {
			Prompt.warning("No legal targets", card.getOwner());
			return;
		}
		Card[] e = (Card[]) d.toArray();
		Card f = Prompt.pick(e, 1, card.getOwner());
		f.getOwner().stageToHand(f);
		EventHandler.actAbilityActivated(this);
	}

	public Card bound() {
		return card;
	}

	public boolean check() {
		return card.getOwner().hasStock(4, this);
	}

}
