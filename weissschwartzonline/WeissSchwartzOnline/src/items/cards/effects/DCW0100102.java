package items.cards.effects;

import client.Prompt;
import server.EventHandler;
import items.cards.Card;
import items.cards.effectInterface.Activated;
import items.cards.effectInterface.DispellOnEndTurn;
import items.cards.Character;

public class DCW0100102 implements Activated, DispellOnEndTurn {
	private Card card;
	private Character target;

	@Override
	public void bind(Card c) {
		card = c;

	}

	@Override
	public void payCost() {
		card.getOwner().pay(1, this);
		card.setState(Card.REST);
		EventHandler.characterRested(card);
	}

	@Override
	public void activate() {
		try {
			Character[] c = card.getOwner().searchStageByTrait("Magic");
			Character k = (Character) Prompt.pick(c, 1, card.getOwner());
			target = k;
			target.giveSoul(1);
		} catch (Exception e) {
			Prompt.warning("No valid targets", card.getOwner());
		}
		EventHandler.actAbilityActivated(this);

	}

	@Override
	public boolean check() {
		return card.getOwner().hasStock(1, this)
				&& card.getState() == Card.STAND;
	}

	@Override
	public void dispell() {
		if (target != null && target.onField()) {
			target.giveSoul(-1);
		}
	}

	@Override
	public Card bound() {
		return card;
	}

}
