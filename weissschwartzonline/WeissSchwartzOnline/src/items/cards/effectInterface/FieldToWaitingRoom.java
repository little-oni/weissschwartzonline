package items.cards.effectInterface;

import items.cards.Card;

public interface FieldToWaitingRoom extends Effect {
	public boolean check(Card c);
}
