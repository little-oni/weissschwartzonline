package items.cards.effectInterface;

import items.cards.Card;

public interface FieldToHand extends Effect {
	public boolean check(Card c);
}
