package items.cards.effectInterface;

import items.field.Player;

public interface LevelUp extends Effect {
	public boolean check(Player p);
}
