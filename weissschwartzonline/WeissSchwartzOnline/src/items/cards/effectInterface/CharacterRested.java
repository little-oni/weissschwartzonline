package items.cards.effectInterface;

import items.cards.Card;

public interface CharacterRested extends Effect {
	public boolean check(Card c);
}
