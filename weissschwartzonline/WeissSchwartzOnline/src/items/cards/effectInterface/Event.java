package items.cards.effectInterface;

public interface Event extends Effect{
	public boolean check();
}
