package items.cards.effectInterface;

import items.cards.Card;

public interface Effect {
	public void bind(Card c);

	public void payCost();

	public void activate();

	public Card bound();
}
