package items.cards.effectInterface;

public interface Activated extends Effect {
	public boolean check();
}
