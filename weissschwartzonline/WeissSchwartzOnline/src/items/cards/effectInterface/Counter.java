package items.cards.effectInterface;

public interface Counter extends Effect{
	public boolean check();
}
