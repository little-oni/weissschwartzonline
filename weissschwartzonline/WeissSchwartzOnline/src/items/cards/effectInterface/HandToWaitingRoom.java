package items.cards.effectInterface;

import items.cards.Card;

public interface HandToWaitingRoom extends Effect{
	public boolean check (Card c);
}
