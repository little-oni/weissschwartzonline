package items.cards.effectInterface;

public interface Condition extends Effect{
	public boolean check();
}
