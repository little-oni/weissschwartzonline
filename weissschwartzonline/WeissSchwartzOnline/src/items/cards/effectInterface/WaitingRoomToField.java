package items.cards.effectInterface;

import items.cards.Card;

public interface WaitingRoomToField extends Effect{
	public boolean check(Card c);
}
