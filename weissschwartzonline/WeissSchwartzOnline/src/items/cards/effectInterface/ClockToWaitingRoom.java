package items.cards.effectInterface;

import items.cards.Card;

public interface ClockToWaitingRoom extends Effect{
	public boolean check(Card c);
}
