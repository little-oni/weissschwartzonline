package items.cards.effectInterface;

public interface AutoAbilityActivated extends Effect{
	public boolean check(Effect c);
}
