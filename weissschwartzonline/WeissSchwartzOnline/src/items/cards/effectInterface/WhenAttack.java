package items.cards.effectInterface;

import items.cards.Card;

public interface WhenAttack extends Effect{
	public boolean check(Card c);
}
