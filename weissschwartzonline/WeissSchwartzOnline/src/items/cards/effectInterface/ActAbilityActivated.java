package items.cards.effectInterface;

public interface ActAbilityActivated extends Effect {
	public boolean check(Effect c);
}
