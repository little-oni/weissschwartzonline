package items.interfaces;

public interface I_Effect {
	public boolean condition();
	public void cost();
	public void resolve();
}
