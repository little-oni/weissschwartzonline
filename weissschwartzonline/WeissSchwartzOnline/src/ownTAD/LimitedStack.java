package ownTAD;

import java.util.Stack;

public class LimitedStack<E> extends Stack<E> {
	private int max = 7;

	public LimitedStack(int i) {
		max = i;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public E push(E elem) {
		if (size() >= max)
			return super.push(elem);
		return null;
	}

}
