import server.Game;
import exceptions.Replace;
import items.cards.Character;
import items.field.Deck;
import items.field.Player;
import items.field.Stage;

public class Test {
	public static void main(String[] args) {
		Player p1 = new Player();
		Player p2 = new Player();
		Game g = new Game(p1, p2);
		System.out.println(g.oponent(p2) == p1);
	}

}
